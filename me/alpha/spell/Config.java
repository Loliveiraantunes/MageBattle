package me.alpha.spell;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;

public class Config implements Listener {

	
	 public  static File configFile;
	 public static File groupsFile;
	 public static File historyFile;
	 public static FileConfiguration config;
	 public static FileConfiguration groups;
	 public static FileConfiguration history;
	
	
	
	static main plugin = main.getPlugin(main.class);
	
	public static void LoadConfig(){
		File DataFolder = plugin.getDataFolder();
		configFile = new File(DataFolder,"config.yml");
		groupsFile = new File(DataFolder,"groups.yml");
		historyFile = new File(DataFolder,"history.yml");
	}
	
	public static void CreateFile(){
		
		try {
			if(!(configFile.exists())){
				configFile.getParentFile().mkdirs();
				//copy(plugin.getResource("config.yml"),configFile);
			}
			if(!(groupsFile.exists())){
				groupsFile.getParentFile().mkdirs();
			//	copy(plugin.getResource("groups.yml"),groupsFile);
			}
			if(!(historyFile.exists())){
				historyFile.getParentFile().mkdirs();
			//	copy(plugin.getResource("history"),historyFile);
			}
		} catch (Exception e) {
			  e.printStackTrace();
		}
		
	
		
		config = new YamlConfiguration();
	    groups = new YamlConfiguration();
	    history = new YamlConfiguration();
	    loadYamls();
		
		
	}
	
	public static void saveYamls() {
	    try {
	        config.save(configFile);
	        groups.save(groupsFile);
	        history.save(historyFile);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	public static void loadYamls() {
	    try {
	        config.load(configFile);
	        groups.load(groupsFile);
	        history.load(historyFile);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
	
	
	
	
	public static void copy(java.io.InputStream inputStream,File file){
		try{
			OutputStream out = new FileOutputStream(file);
			byte[] buff = new byte[1024];
			int len;
			while((len= inputStream.read(buff))>0){
				out.write(buff,0,len);
			}
			out.close();
			inputStream.close();
			
		}catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}		
		
	}
	

	
	
	
}
