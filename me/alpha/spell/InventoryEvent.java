package me.alpha.spell;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryEvent implements Listener {

	public static ItemStack lobby() {
		ItemStack lobby = new ItemStack(Material.EMERALD);
		ItemMeta lMeta = lobby.getItemMeta();
		ArrayList<String> lore = new ArrayList<>();
		lore.add("" + ChatColor.GREEN + "use este item para Selecionar sua Classe");
		lMeta.setLore(lore);
		lMeta.setDisplayName("" + ChatColor.GREEN + "" + ChatColor.BOLD + "Classes");
		lMeta.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 10, true);
		lobby.setItemMeta(lMeta);
		return lobby;
	}

	public static ItemStack WitherMage() {

		ItemStack MagoS = new ItemStack(Material.ENDER_PEARL);
		ItemMeta MagoSmeta = MagoS.getItemMeta();
		MagoSmeta.setDisplayName("" + ChatColor.BOLD + "" + ChatColor.DARK_PURPLE + "Wither Mage");
		MagoSmeta.addEnchant(Enchantment.THORNS, 10, true);
		ArrayList<String> Lore = new ArrayList<>();
		Lore.add("" + ChatColor.DARK_GREEN + "Classe mago das Sombras... Classe Stealth");
		MagoSmeta.setLore(Lore);
		MagoS.setItemMeta(MagoSmeta);
		return MagoS;

	}

	public static ItemStack Barreira() {

		ItemStack Barreira = new ItemStack(Material.STAINED_GLASS_PANE);
		ItemMeta BarreiraM = Barreira.getItemMeta();
		BarreiraM.setDisplayName("" + ChatColor.BOLD + "" + ChatColor.RED + "=== X ===");

		Barreira.setItemMeta(BarreiraM);
		return Barreira;

	}

	public static ItemStack Back() {
		ItemStack back = new ItemStack(Material.PAPER);
		ItemMeta backM = back.getItemMeta();
		backM.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Sair da Classe");
		back.setItemMeta(backM);
		return back;

	}

	public static ItemStack ChickenMaster() {
		ItemStack MagoS = new ItemStack(Material.EGG);
		ItemMeta MagoSmeta = MagoS.getItemMeta();
		MagoSmeta.setDisplayName("" + ChatColor.BOLD + "" + ChatColor.WHITE + "ChickenMaster");
		MagoSmeta.addEnchant(Enchantment.THORNS, 10, true);
		ArrayList<String> Lore = new ArrayList<>();
		Lore.add("" + ChatColor.DARK_GREEN + "Classe mago das Sombras... Classe Dano");
		MagoSmeta.setLore(Lore);
		MagoS.setItemMeta(MagoSmeta);
		return MagoS;
	}
	
	
	public static ItemStack IceBeast() {
		ItemStack MagoS = new ItemStack(Material.SNOW_BALL);
		ItemMeta MagoSmeta = MagoS.getItemMeta();
		MagoSmeta.setDisplayName("" + ChatColor.BOLD + "" + ChatColor.AQUA + "IceBeast");
		MagoSmeta.addEnchant(Enchantment.THORNS, 10, true);
		ArrayList<String> Lore = new ArrayList<>();
		Lore.add("" + ChatColor.GRAY + "This mage is a Tank.");
		MagoSmeta.setLore(Lore);
		MagoS.setItemMeta(MagoSmeta);
		return MagoS;
	}
	
	public static ItemStack BurningLover() {
		ItemStack MagoS = new ItemStack(Material.BLAZE_POWDER);
		ItemMeta MagoSmeta = MagoS.getItemMeta();
		MagoSmeta.setDisplayName("" + ChatColor.BOLD + "" + ChatColor.GOLD + "BurningLover");
		MagoSmeta.addEnchant(Enchantment.THORNS, 10, true);
		ArrayList<String> Lore = new ArrayList<>();
		Lore.add("" + ChatColor.GRAY + "This mage is a Damager.");
		MagoSmeta.setLore(Lore);
		MagoS.setItemMeta(MagoSmeta);
		return MagoS;
	}
	
	

	public static Inventory ClassInv = Bukkit.createInventory(null, 9,
			"" + ChatColor.BOLD + "" + ChatColor.DARK_PURPLE + "Classes");

	static {

		ClassInv.setItem(0, InventoryEvent.WitherMage());
		ClassInv.setItem(1, InventoryEvent.Barreira());
		ClassInv.setItem(2, InventoryEvent.ChickenMaster());
		ClassInv.setItem(3, InventoryEvent.Barreira());
		ClassInv.setItem(4, InventoryEvent.IceBeast());
		ClassInv.setItem(5, InventoryEvent.Barreira());
		ClassInv.setItem(6, InventoryEvent.BurningLover());
	}

	@EventHandler(priority=EventPriority.HIGH)
	public void InventarioTeste(InventoryClickEvent e) {
		Player pl = (Player) e.getWhoClicked();
		ItemStack clicked = e.getCurrentItem();

		if (e.getInventory().getName() == ClassInv.getName()) {

			if (clicked.getType() == InventoryEvent.WitherMage().getType()) {
				Eventos.EscolherClasse(pl, Eventos.ClasseWM);
				Eventos.PegarKit(pl, Eventos.ClasseWM);
				pl.closeInventory();
			}

			if (clicked.getType() == InventoryEvent.ChickenMaster().getType()) {
				Eventos.EscolherClasse(pl, Eventos.ClasseCM);
				Eventos.PegarKit(pl, Eventos.ClasseCM);
				pl.closeInventory();
			}
			
			
			if (clicked.getType() == InventoryEvent.IceBeast().getType()) {
				Eventos.EscolherClasse(pl, Eventos.ClasseIB);
				Eventos.PegarKit(pl, Eventos.ClasseIB);
				pl.closeInventory();
				
				
			}

			if (clicked.getType() == InventoryEvent.BurningLover().getType()) {
				Eventos.EscolherClasse(pl, Eventos.ClasseBL);
				Eventos.PegarKit(pl, Eventos.ClasseBL);
				pl.closeInventory();
				
				
			}
			
			e.setCancelled(true);
		}

	}

}
