package me.alpha.spell;


import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class setArena implements Listener {

	public static ItemStack getADM() {
		ItemStack ADM = new ItemStack(Material.STAINED_CLAY, 1, (short) 2);
		ItemMeta ADM_META = ADM.getItemMeta();
		ADM_META.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "ADM");
		ADM.setDurability((short) 5);
		ADM.setItemMeta(ADM_META);
		return ADM;
	}
	
	public static ItemStack CreateArena() {
		ItemStack ADM = new ItemStack(Material.STAINED_CLAY, 1, (short) 2);
		ItemMeta ADM_META = ADM.getItemMeta();
		ADM_META.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "Create Arena");
		ADM.setDurability((short) 5);
		ADM.setItemMeta(ADM_META);
		return ADM;
	}
	
	public static ItemStack Create() {
		ItemStack ADM = new ItemStack(Material.STAINED_CLAY, 1);
		ItemMeta ADM_META = ADM.getItemMeta();
		ADM_META.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "Create");
		ADM.setDurability((short) 5);
		ADM.setItemMeta(ADM_META);
		return ADM;
	}
	public static ItemStack DeletArena() {
		ItemStack ADM = new ItemStack(Material.STAINED_CLAY, 1, (short) 2);
		ItemMeta ADM_META = ADM.getItemMeta();
		ADM_META.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "Delete Arena");
		ADM.setDurability((short) 14);
		ADM.setItemMeta(ADM_META);
		return ADM;
	}
	public static ItemStack EditArena() {
		ItemStack ADM = new ItemStack(Material.STAINED_CLAY, 1, (short) 2);
		ItemMeta ADM_META = ADM.getItemMeta();
		ADM_META.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "Edit Arena");
		ADM.setDurability((short) 4);
		ADM.setItemMeta(ADM_META);
		return ADM;
	}
	
	
	public static ItemStack listArena() {
		ItemStack ADM = new ItemStack(Material.STAINED_CLAY, 1, (short) 2);
		ItemMeta ADM_META = ADM.getItemMeta();
		ADM_META.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "List Arena");
		ADM.setDurability((short) 3);
		ADM.setItemMeta(ADM_META);
		return ADM;
	}

	public static ItemStack SetNome() {
		ItemStack ADM = new ItemStack(Material.PAPER);
		ItemMeta ADM_META = ADM.getItemMeta();
		ADM_META.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "do /Rename ''Name''. Arena Name");
		ADM.setItemMeta(ADM_META);
		return ADM;
	}
	
	public static ItemStack addSpawn() {
		ItemStack ADM = new ItemStack(Material.STAINED_CLAY);
		ItemMeta ADM_META = ADM.getItemMeta();
		ADM_META.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "Add Spawn");
		ADM.setDurability((short)11);
		ADM.setItemMeta(ADM_META);
		return ADM;
	}
	
	
	public static ItemStack ListSpawn() {
		ItemStack ADM = new ItemStack(Material.STAINED_CLAY);
		ItemMeta ADM_META = ADM.getItemMeta();
		ADM_META.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "List Spawn");
		ADM.setDurability((short)4);
		ADM.setItemMeta(ADM_META);
		return ADM;
	}
	
	public static ItemStack tpSpawn() {
		ItemStack ADM = new ItemStack(Material.STAINED_CLAY);
		ItemMeta ADM_META = ADM.getItemMeta();
		ADM_META.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "Teleport to Spawn");
		ADM.setDurability((short)9);
		ADM.setItemMeta(ADM_META);
		return ADM;
	}
	
	
	public static ItemStack removeSpawn() {
		ItemStack ADM = new ItemStack(Material.STAINED_CLAY);
		ItemMeta ADM_META = ADM.getItemMeta();
		ADM_META.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "Remove Spawn");
		ADM.setDurability((short)14);
		ADM.setItemMeta(ADM_META);
		return ADM;
	}
	
	
	
	
	public static ItemStack Exit() {
		ItemStack ADM = new ItemStack(Material.BARRIER);
		ItemMeta ADM_META = ADM.getItemMeta();
		ADM_META.setDisplayName("" + ChatColor.RED + ChatColor.BOLD + "Exit");
		ADM.setItemMeta(ADM_META);
		return ADM;
	}
	
	
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void CreateArena(PlayerInteractEvent e){
		Player pl =e.getPlayer();
		if(pl.getItemInHand().isSimilar(getADM())){
			pl.getInventory().clear();
			pl.getInventory().setItem(1,CreateArena());
			pl.getInventory().setItem(2,listArena());
			pl.getInventory().setItem(3,EditArena());
			pl.getInventory().setItem(4,DeletArena());
			pl.getInventory().setItem(8,Exit());
			e.setCancelled(true);
		}
		
		
		
		if(pl.getItemInHand().isSimilar(CreateArena())){
			pl.getInventory().clear();
			pl.getInventory().setItem(0,SetNome());
			pl.getInventory().setItem(5,Create());
			pl.getInventory().setItem(8,Exit());
			e.setCancelled(true);
		}
		
		if(pl.getItemInHand().isSimilar(Create())){
			e.setCancelled(true);
			for(ItemStack item: pl.getInventory().getContents()){
				
				if(item.getType()==Material.PAPER){
				pl.sendMessage(item.getItemMeta().getDisplayName());	
				
				pl.getInventory().clear();
				pl.getInventory().setItem(0,item);
				pl.getInventory().setItem(2,addSpawn());
				pl.getInventory().setItem(3,removeSpawn());
				pl.getInventory().setItem(4,ListSpawn());
				pl.getInventory().setItem(5,tpSpawn());
				
				pl.getInventory().setItem(8,Exit());
				}
				
			}
		
		
			e.setCancelled(true);
		}
		if(pl.getItemInHand().isSimilar(Exit())){
			
		pl.getInventory().clear();
		
		
		}
		
		
	}
	
	
	@EventHandler
	public void placeBlock(BlockPlaceEvent e) {
		ItemStack b =  e.getItemInHand();
		
		if(b.isSimilar(getADM()) || b.isSimilar(Create()) || b.isSimilar(CreateArena()) ||
				b.isSimilar(DeletArena()) || b.isSimilar(addSpawn()) ||
				b.isSimilar(listArena()) || b.isSimilar(EditArena())|| b.isSimilar(Exit()) ){
			e.setCancelled(true);
		}
	}
	
	
	
	
	


}
