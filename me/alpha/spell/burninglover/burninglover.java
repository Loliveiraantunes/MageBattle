package me.alpha.spell.burninglover;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import me.alpha.spell.Eventos;
import me.alpha.spell.main;
import net.md_5.bungee.api.ChatColor;


public class burninglover implements Listener {

	 main plugin = main.getPlugin(main.class);

	public static ItemStack staff() {
		ItemStack Staff = new ItemStack(Material.BLAZE_ROD);
		ItemMeta StaffM = Staff.getItemMeta();
		StaffM.setDisplayName("" + ChatColor.GOLD +""+ ChatColor.BOLD + "Burning Staff");
		StaffM.addEnchant(Enchantment.DAMAGE_ALL, 2, true);
		StaffM.addEnchant(Enchantment.FIRE_ASPECT, 2, true);
		Staff.setItemMeta(StaffM);
		return Staff;
	}

	public static ItemStack Explosion() {
		ItemStack Staff = new ItemStack(Material.BLAZE_POWDER);
		ItemMeta StaffM = Staff.getItemMeta();
		StaffM.setDisplayName("" + ChatColor.RED +""+ ChatColor.BOLD + "ExplosionField");
		StaffM.addEnchant(Enchantment.PROTECTION_FIRE, 2, true);
		Staff.setItemMeta(StaffM);
		return Staff;
	}


	public static ItemStack DashFire() {
		ItemStack Staff = new ItemStack(Material.GOLD_INGOT);
		ItemMeta StaffM = Staff.getItemMeta();
		StaffM.setDisplayName("" + ChatColor.GOLD +""+ ChatColor.BOLD + "FireDash");
		StaffM.addEnchant(Enchantment.PROTECTION_FIRE, 2, true);
		Staff.setItemMeta(StaffM);
		return Staff;
	}
	
	public static ItemStack blaze() {
		ItemStack Staff = new ItemStack(Material.BLAZE_POWDER);
		ItemMeta StaffM = Staff.getItemMeta();
		StaffM.setDisplayName("powder");
		Staff.setItemMeta(StaffM);
		return Staff;
	}


	@SuppressWarnings("deprecation")
	public static void PrimaryAtk(Player pl, Action action) {
		
		if (!(action == Action.LEFT_CLICK_AIR || action == Action.LEFT_CLICK_BLOCK)  ) {
			if (pl.getItemInHand().equals(staff()) && pl.getExp() >= 0.25) {
		Fireball fb = pl.launchProjectile(Fireball.class);
		fb.setCustomName("Fire");
		fb.setIsIncendiary(false);
		fb.setVelocity(pl.getEyeLocation().getDirection().multiply(1));
		fb.setGravity(true);
		pl.getWorld().playSound(pl.getLocation(), Sound.BLOCK_REDSTONE_TORCH_BURNOUT, 1f, 1f);
		pl.getWorld().spawnParticle(Particle.HEART, pl.getLocation(), 3, 0.5, 1, 0.5, 0.1);
		Eventos.Mana(0.25F, pl);
			} else {
				Eventos.lowMana(0.25f, pl, staff());
			}
		}
		
	}
	
	@SuppressWarnings("deprecation")
	public static void SetDef(final Player pl , Action action){
		if (!(action == Action.LEFT_CLICK_AIR || action == Action.LEFT_CLICK_BLOCK)  ) {
			if (pl.getItemInHand().isSimilar(Explosion()) && pl.getExp() >= 0.25) {
					pl.getInventory().removeItem(Explosion());
					pl.getWorld().spawnParticle(Particle.LAVA, pl.getLocation(), 20, 3, 0, 3, 0.1);
					pl.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, pl.getLocation(), 20, 3, 0, 3, 0.1);
					pl.getWorld().playSound(pl.getLocation(), Sound.BLOCK_REDSTONE_TORCH_BURNOUT, 1f, 1f);
					for(Entity ent : pl.getNearbyEntities(3, 1, 3)){
						ent.setVelocity((ent.getLocation().getDirection().multiply(-1).setY(1)));
						ent.getWorld().spawnParticle(Particle.HEART, ent.getLocation().getX(),ent.getLocation().getY(), ent.getLocation().getZ(), 5, 0.5, 1, 0.5, 0.01);
						ent.setFireTicks(5*20);
						ent.getWorld().playSound(ent.getLocation(), Sound.ENTITY_BLAZE_HURT, 0.8f, 0.8f);
						((Damageable) ent).damage(6F,pl);
					}
					
					Eventos.Mana(0.25f,pl);
					pl.getInventory().addItem(Explosion());

				}else{
				Eventos.lowMana(0.25f,pl,Explosion());
				}
			}
	}


	static ArrayList<BlockState> blockp = new ArrayList<>();
	@SuppressWarnings("deprecation")
	public static void DashFire(final Player pl , Action action ,final Plugin plugin) {


		if (!(action == Action.LEFT_CLICK_AIR || action == Action.LEFT_CLICK_BLOCK)) {
			if (pl.getItemInHand().isSimilar(DashFire()) && pl.getExp() >= 0.25) {
				pl.getWorld().playSound(pl.getLocation(),Sound.BLOCK_FIRE_EXTINGUISH,1f,1f);
				pl.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE,3*20,5),true);
				pl.setVelocity(pl.getLocation().getDirection().multiply(2).setY(0.5));
				pl.getInventory().removeItem(DashFire());
				Eventos.Mana(0.25F,pl);
				final int[] cont = {0};
				new BukkitRunnable() {
					int i = 0;

					@Override
					public void run() {

						Location loc = pl.getLocation();
						double x = loc.getX();
						double y = loc.getY();
						double z = loc.getZ();

						for (float cx= 0; cx < 3; cx++) {
							for (float cy = 0; cy < 10; cy++) {
								for (float cz = 0; cz < 3; cz++) {
									loc.setX(x-cx);
									loc.setY(y - cy);
									loc.setZ(z-cz);

									if (loc.getBlock().getType() == Material.AIR) {
										blockp.add(loc.getBlock().getState());
										loc.getBlock().setType(Material.FIRE);
										cont[0] +=1;
									}
								}
							}




						}
						new BukkitRunnable(){
							@Override
							public void run(){
								try {
									for(int i = 0;i < blockp.size();i++ ){

										if(blockp.contains(blockp.get(i))) {
											Block b =blockp.get(i).getBlock();
											b.setType(Material.AIR);
											blockp.remove(i);
										}
									}
								}catch (Exception e){
									System.out.print("deu Ruim");
								}
								pl.setFireTicks(0);
							}
						}.runTaskLater(plugin,2*20);

						i++;
						if(i > 10){

							pl.getInventory().addItem(DashFire());
							this.cancel();
						}

					}
				}.runTaskTimer(plugin, 0, 1);




			}else{
				Eventos.lowMana(0.25F,pl,DashFire());
			}
		}

	}


	
	static ArrayList<Object> Projetil = new ArrayList<>();

	public static void ProjectFlying(final Projectile e,final Plugin plugin) {
		Projetil.add(e.getUniqueId());
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				if(Projetil.contains(e.getUniqueId()) && e.getCustomName() == "Fire" ){
				e.getWorld().spawnParticle(Particle.LAVA, e.getLocation().getX(),e.getLocation().getY(),e.getLocation().getZ(), 5, 0, 0, 0, 1);
				i++;
				if(i > 100){
					e.remove();
					Projetil.remove(e);
					this.cancel();
				}
				}
			}
		}.runTaskTimer(plugin, 0, 1);
	}


	public static void ItemDrop(final ItemSpawnEvent e, Plugin plugin) {
		if (e.getEntity().getItemStack().isSimilar(blaze())) {
			new BukkitRunnable() {

				@Override
				public void run() {
					e.getEntity().remove();
				}
			}.runTaskLater(plugin, 25);
		}

	}


	public static void Nostack(ItemMergeEvent e) {
		if (e.getEntity().getItemStack().equals(blaze())) {
			e.setCancelled(true);
		}
	}


	public static void NoPick(PlayerPickupItemEvent e) {
		if (e.getItem().getItemStack().equals(blaze())) {
			e.setCancelled(true);
			e.getItem().remove();
		}

	}

	

	public static void ProjectileHit(final ProjectileHitEvent e,Plugin plugin) {
		if (e.getEntity().getShooter() instanceof Player) {
		final Player pl = (Player) e.getEntity().getShooter();
		
		if (e.getEntity().getCustomName() == "Fire") {
			e.getEntity().remove();
			for (int i = 0; i < 10; i++) {
				Random rand = new Random();
				e.getEntity().getWorld().dropItem(
						e.getEntity().getLocation().add(rand.nextInt(2), rand.nextInt(2), rand.nextInt(2)), blaze());
				e.getEntity().getWorld().spawnParticle(Particle.LAVA, e.getEntity().getLocation(), 5, 1, 0.5, 1, 0.1);
			}

			for (Entity ent : e.getEntity().getNearbyEntities(1, 1, 1)) {
				if (ent instanceof LivingEntity) {
					((LivingEntity) ent).damage(10,pl);
				}
			}

			
			
			new BukkitRunnable() {
				
				@Override
				public void run() {
				Projetil.remove(e.getEntity().getUniqueId());
				}
			}.runTaskLater(plugin, 2);
			
		}
			
			
		}

	}

}
