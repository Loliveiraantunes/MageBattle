package me.alpha.spell.wither;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.WitherSkull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import me.alpha.spell.Eventos;
import me.alpha.spell.FireWorkEvents;
import me.alpha.spell.main;

public class WitherMage implements Listener {

	private main plugin = main.getPlugin(main.class);

	public static ItemStack Varinha() {
		ItemStack Staff = new ItemStack(Material.STICK);
		ItemMeta meta = Staff.getItemMeta();
		meta.setDisplayName("" + ChatColor.GREEN + "" + ChatColor.BOLD + "Wither Staff");
		meta.addEnchant(Enchantment.DAMAGE_ALL, 2, true);
		meta.addEnchant(Enchantment.KNOCKBACK, 2, true);
		Staff.setItemMeta(meta);
		return Staff;

	}

	public static ItemStack EyesInvi() {
		ItemStack EyesInv = new ItemStack(Material.EYE_OF_ENDER);
		ItemMeta EyesInvM = EyesInv.getItemMeta();
		EyesInvM.setDisplayName("" + ChatColor.WHITE + "Disappear");
		EyesInvM.addEnchant(Enchantment.DURABILITY, 10, true);
		EyesInv.setItemMeta(EyesInvM);
		return EyesInv;
	}

	public static ItemStack DefWither() {
		ItemStack Staff = new ItemStack(Material.COAL);
		ItemMeta meta = Staff.getItemMeta();
		meta.setDisplayName("" + ChatColor.GRAY + "" + "DeathField");
		meta.addEnchant(Enchantment.FROST_WALKER, 1, true);
		Staff.setItemMeta(meta);
		return Staff;
	}

	@SuppressWarnings("deprecation")
	public static void PrimaryAttack(Player pl, Action action) {

		if (pl.getItemInHand().equals(WitherMage.Varinha()) && pl.getExp() >= 0.25
				&& !(action == Action.LEFT_CLICK_AIR) && !(action == Action.LEFT_CLICK_BLOCK)) {

			Eventos.Mana((float) 0.25, pl);

			for (int i = 0; i < 5; i++) {
				WitherSkull skull = pl.launchProjectile(WitherSkull.class);
				skull.setCustomName("Skull");
			}
		} else {
			Eventos.lowMana(0.25F, pl, WitherMage.Varinha());
		}

	}


@SuppressWarnings("deprecation")
	public  static void SetInv(final Player pl, Action action , Plugin plugin) {
		if (pl.getItemInHand().equals(WitherMage.EyesInvi()) && pl.getExp() >= 0.30 && !(action == Action.LEFT_CLICK_AIR) && !(action == Action.LEFT_CLICK_BLOCK)) {

			Eventos.Mana(0.30f, pl);
			for (int i = 0; i < 3; i++) {
				Random rand = new Random();
				FireWorkEvents
						.FwHit(pl.getLocation().add(new Vector(rand.nextInt(2), rand.nextInt(2), rand.nextInt(2))));
			}

			pl.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 15), true);
			pl.getInventory().remove(EyesInvi());

			final ArrayList<ItemStack> Item = new ArrayList<>();
			Item.add(pl.getInventory().getHelmet());
			Item.add(pl.getInventory().getChestplate());
			Item.add(pl.getInventory().getLeggings());
			Item.add(pl.getInventory().getBoots());
			pl.getInventory().setHelmet(new ItemStack(Material.AIR));
			pl.getInventory().setChestplate(new ItemStack(Material.AIR));
			pl.getInventory().setLeggings(new ItemStack(Material.AIR));
			pl.getInventory().setBoots(new ItemStack(Material.AIR));

			new BukkitRunnable() {

				@Override
				public void run() {
					pl.removePotionEffect(PotionEffectType.INVISIBILITY);
					pl.getInventory().addItem(EyesInvi());
					pl.getInventory().setHelmet(Item.get(0));
					pl.getInventory().setChestplate(Item.get(1));
					pl.getInventory().setLeggings(Item.get(2));
					pl.getInventory().setBoots(Item.get(3));
					Item.clear();
					FireWorkEvents.FwHit(pl.getLocation());
				}
			}.runTaskLater(plugin, 7 * 20);



		} else {
			Eventos.lowMana(0.30F, pl, WitherMage.EyesInvi());
		}

	}

	@SuppressWarnings("deprecation")
	public static void SetDef(final Player pl, Action action, Plugin plugin) {
		if (pl.getItemInHand().equals(WitherMage.DefWither()) && pl.getExp() >= 0.30
				&& !(action == Action.LEFT_CLICK_AIR) && !(action == Action.LEFT_CLICK_BLOCK)) {
			pl.getWorld().playSound(pl.getLocation(),Sound.BLOCK_GLASS_BREAK,1f,1f);
			Eventos.Mana((float) 0.30, pl);
			new BukkitRunnable() {
				int i = 0;

				@Override
				public void run() {

					pl.getLocation().getWorld().spawnParticle(Particle.SMOKE_NORMAL, pl.getLocation(), 100, 3, 3, 3, 0);
					pl.getLocation().getWorld().spawnParticle(Particle.CRIT, pl.getLocation().getX(),
							pl.getLocation().getY() + 1.5, pl.getLocation().getZ(), 10, 3, 0, 3, 0.01);

					for (Entity e : pl.getNearbyEntities(3, 4, 3)) {
						if (e instanceof LivingEntity) {

							if (!Eventos.checkteam(pl).contains(e.getName())) {
								((Damageable) e).damage(2, pl);
								pl.getLocation().getWorld().spawnParticle(Particle.FIREWORKS_SPARK, e.getLocation().getX(),
										e.getLocation().getY(), e.getLocation().getZ(), 2, 0.2, 1, 0.2, 0.01);
								pl.getWorld().playSound(e.getLocation(),Sound.ENTITY_BAT_HURT,1f,1f);
							}

						}
					}

					if (i < 200) {
						i++;
					} else {
						pl.getWorld().playSound(pl.getLocation(),Sound.BLOCK_CLOTH_PLACE,1f,1f);
						this.cancel();
					}
				}
			}.runTaskTimer(plugin, 0, 1);


		} else {
			Eventos.lowMana(0.30F, pl, WitherMage.DefWither());
		}

	}



	@SuppressWarnings("deprecation")
	public static void DanoComVarinha(EntityDamageByEntityEvent e) {

		Entity pl = e.getDamager();
		Entity ent = e.getEntity();
		if (ent instanceof LivingEntity && pl instanceof Player) {

			if (((HumanEntity) pl).getItemInHand().equals(Varinha()))

				((LivingEntity) ent).addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 6 * 20, 1), true);

			ent.getLocation().getWorld().spawnParticle(Particle.FIREWORKS_SPARK, ent.getLocation().getX(),
					ent.getLocation().getY(), ent.getLocation().getZ(), 2, 0.2, 1, 0.2, 0.01);
		}
	}

	static ArrayList<Object> Projetil = new ArrayList<>();

	public static void ProjetilVoando(final Projectile pj, final Player pl,Plugin plugin) {

		if (pj.getShooter() instanceof Player) {

			Projetil.add(pj.getUniqueId());
		}
		new BukkitRunnable() {

			@Override
			public void run() {
				int i = 0;
				if (Projetil.contains(pj.getUniqueId()) && pj.getCustomName() == "Skull") {
					// pl.spawnParticle(Particle.VILLAGER_HAPPY,
					// pj.getLocation(), 2);
					pl.getLocation().getWorld().spawnParticle(Particle.SMOKE_LARGE, pj.getLocation().getX(),
							pj.getLocation().getY(), pj.getLocation().getZ(), 2, 0, 0, 0, 0.01);
					i++;
					if(i > 100){
						pj.remove();
						Projetil.remove(pj);
						this.cancel();
					}
				}
			}
		}.runTaskTimer(plugin, 0, 1);
	}


	public static void projectHit(ProjectileHitEvent e,Plugin plugin) {
		if (e.getEntity().getShooter() instanceof Player) {

			final Player pl = (Player) e.getEntity().getShooter();
			final Projectile pj = e.getEntity();
			if (pj.getCustomName() == "Skull") {

				for (Entity ent : pj.getNearbyEntities(1, 1, 1)) {

					if (ent instanceof LivingEntity) {

						((LivingEntity) ent).damage(10, pl);
						pl.playSound(pj.getLocation(), Sound.ENTITY_WITHER_SKELETON_HURT, 1f, 1f);
					}

				}

				new BukkitRunnable() {

					@Override
					public void run() {
						FireWorkEvents.FwHit(pj.getLocation());
					}
				}.runTaskLater(plugin, 1);
				Projetil.remove(pj.getUniqueId());
			}
		}

	}

}
