
package me.alpha.spell;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin implements Listener {
	static ArrayList<Object> PlayerList = new ArrayList<>();
	public Plugin p = this;
	public PluginManager pm = this.getServer().getPluginManager();

	@Override
	public void onLoad() {
		this.getServer().broadcastMessage(
				"" + ChatColor.GREEN + " --------------- Loading.... ------------");
		this.getServer().broadcastMessage(
			   	"" + ChatColor.GOLD +  " ----------------------------------------");
		this.getServer().broadcastMessage("" + ChatColor.GREEN + "       Spell V 0.0 ");
		this.getServer().broadcastMessage(
				"" + ChatColor.GOLD +  " -----------------------------------------");
		Config.LoadConfig();

	}

	@Override
	public void onEnable() {
		pm.registerEvents(this, this);
		pm.registerEvents(new Eventos(), this);
		pm.registerEvents(new util(), this);
		pm.registerEvents(new ListenersEvent(), this);
		pm.registerEvents(new setArena(), this);
		this.getServer().getLogger()
				.info("" + ChatColor.GREEN + " ------------------------ Cheking config.... ------------------------");

		Config.CreateFile();
		this.getServer().getLogger()
				.info("" + ChatColor.GREEN + " ------------------------ Cheking Warps.... ------------------------");

	}

	@Override
	public void onDisable() {

		HandlerList.unregisterAll();
		PlayerList.clear();
		Eventos.clearClasses();
	}
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args) {
		if (cmdLabel.equalsIgnoreCase("adm")) {
			sender.sendMessage(ChatColor.RED + "item Added.");
			if(sender instanceof Player){
				((Player) sender).getInventory().clear();
				((Player) sender).getInventory().addItem(setArena.getADM());
			}
			return true;
		}
		
		if (cmdLabel.equalsIgnoreCase("rename")) {
			if(sender instanceof Player){
			ItemStack stack=	((Player) sender).getItemInHand();
			ItemMeta stackMeta = stack.getItemMeta();
			stackMeta.setDisplayName(args[0]);
			stack.setItemMeta(stackMeta);
			sender.sendMessage(ChatColor.RED + "Rename to "+ChatColor.GREEN+args[0]);
			}
			return true;
		}
		return true;

	}

	
	
	

	@EventHandler
	public void Desconect(PlayerQuitEvent e) {
		PlayerList.remove(e.getPlayer().getUniqueId());
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void Pegavarinha(PlayerInteractEvent e) {
		Player pl = e.getPlayer();
		if (pl.getItemInHand().equals(InventoryEvent.lobby())) {

			pl.openInventory(InventoryEvent.ClassInv);
		}

	}

}
