package me.alpha.spell;


import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.FireworkEffect.Builder;
import org.bukkit.entity.Firework;
import org.bukkit.event.Listener;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class FireWorkEvents implements Listener {
	
	private static main plugin = main.getPlugin(main.class);
	
	
	
	
	public static Firework FwHit(Location loc) {
		final Firework fw = loc.getWorld().spawn(loc, Firework.class);
		FireworkMeta fwM = fw.getFireworkMeta();
		Builder build = FireworkEffect.builder();
		fwM.addEffect(build.flicker(false).trail(false).withColor(Color.BLACK).build());
		fwM.setPower(10);
		fw.setSilent(true);
		fw.setFireworkMeta(fwM);
		new BukkitRunnable() {
			@Override
			public void run() {
				fw.detonate();
			}
		}.runTaskLater(plugin, 1);
		return fw;
}


	public static Firework FwHitEgg(Location loc) {
		final Firework fw = loc.getWorld().spawn(loc, Firework.class);
		FireworkMeta fwM = fw.getFireworkMeta();
		Builder build = FireworkEffect.builder();
		fwM.addEffect(build.flicker(false).trail(false).withColor(Color.GREEN).build());
		fwM.setPower(10);
		fw.setSilent(true);
		fw.setFireworkMeta(fwM);
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				fw.detonate();
			}
		}.runTaskLater(plugin, 1);
		return fw;
}

	
	
	
	
	
	
	
	
	
	
	
	

}
