package me.alpha.spell.IceBeast;

import java.util.ArrayList;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import me.alpha.spell.Eventos;
import me.alpha.spell.main;
import net.md_5.bungee.api.ChatColor;



public class icebeast implements Listener {

	public main plugin = main.getPlugin(main.class);

	public static ItemStack varinha() {
		ItemStack Icebeast = new ItemStack(Material.PRISMARINE_SHARD);
		ItemMeta IceMeta = Icebeast.getItemMeta();
		IceMeta.setDisplayName("" + ChatColor.BOLD + "" + ChatColor.DARK_AQUA + "Ice Staff");
		IceMeta.addEnchant(Enchantment.DAMAGE_ALL, 2, true);
		IceMeta.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 5, true);
		Icebeast.setItemMeta(IceMeta);
		return Icebeast;
	}

	public static ItemStack IceField() {
		ItemStack Icebeast = new ItemStack(Material.SUGAR);
		ItemMeta IceMeta = Icebeast.getItemMeta();
		IceMeta.setDisplayName("" + ChatColor.BOLD + "" + ChatColor.AQUA + "IceStorm");
		IceMeta.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 5, true);
		Icebeast.setItemMeta(IceMeta);
		return Icebeast;
	}

	public static ItemStack IceShield() {
		ItemStack Icebeast = new ItemStack(Material.IRON_INGOT);
		ItemMeta IceMeta = Icebeast.getItemMeta();
		IceMeta.setDisplayName("" + ChatColor.BOLD + "" + ChatColor.WHITE + "Ice Armor");
		IceMeta.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 5, true);
		Icebeast.setItemMeta(IceMeta);
		return Icebeast;
	}


	@SuppressWarnings("deprecation")
	public static void PrimaryAttack(final Player pl, org.bukkit.plugin.Plugin plugin, Action action) {
		if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK)
				&& pl.getExp() >= 0.15 && pl.getItemInHand().equals(varinha())) {
			Eventos.Mana(0.15F, pl);


		new BukkitRunnable() {
			int i = 0;

			@Override
			public void run() {
				Snowball snow = pl.launchProjectile(Snowball.class);
				snow.setCustomName("Snow");
				snow.setVelocity(pl.getLocation().getDirection().multiply(1.5));
				pl.playSound(pl.getLocation(), Sound.BLOCK_SNOW_PLACE, 1f, 1f);
				i += 2;
				if (i > 10) {
					this.cancel();
				}

			}
		}.runTaskTimer(plugin, 0, 2);
		} else {
			Eventos.lowMana(0.15F, pl, varinha());
		}
	}
	@SuppressWarnings("deprecation")
	public static void SecondAttack(final Player pl, Plugin plugin,Action action) {
		if ((action == Action.RIGHT_CLICK_AIR ||action == Action.RIGHT_CLICK_BLOCK)
				&& pl.getExp() >= 0.25 && pl.getItemInHand().equals(IceField())) {
			Eventos.Mana(0.25F, pl);


		final int r = 5;

		final Location loc = new Location(pl.getWorld(), pl.getLocation().getX(), pl.getLocation().getY(),
				pl.getLocation().getZ());

		new BukkitRunnable() {
			int i = 0;

			@Override
			public void run() {

				pl.getLocation().getWorld().spawnParticle(Particle.SNOWBALL, loc.getX(), loc.getY() + 8, loc.getZ(),
						150, r, 0, r, 0.01);
				pl.getLocation().getWorld().spawnParticle(Particle.CLOUD, loc.getX(), loc.getY() + 8, loc.getZ(), 40, r,
						0, r, 0.01);

				for (Entity ent : pl.getNearbyEntities(10, 10, 10)) {
					if (ent.getLocation().distance(loc) < 12 && !Eventos.checkteam(pl).contains(ent.getName())) {
						if (ent instanceof LivingEntity) {
							((LivingEntity) ent).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 8 * 20, 2),
									true);
						}
					}
				}

				i++;
				if (i > 150) {
					this.cancel();
				}
			}
		}.runTaskTimer(plugin, 0, 1);
		} else {
			Eventos.lowMana(0.25F, pl, IceField());
		}
	}

	@SuppressWarnings("deprecation")
	public static void Setdef(final Player pl,Plugin plugin , Action action) {
		if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK)
				&& pl.getExp() >= 0.25 && pl.getItemInHand().equals(IceShield())) {
			Eventos.Mana(0.35F, pl);


		pl.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, Integer.MAX_VALUE, 4), true);

		final ItemStack Helmet = new ItemStack(Material.LEATHER_HELMET);
		final ItemStack Chest = new ItemStack(Material.LEATHER_CHESTPLATE);
		final ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
		final ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta hel = (LeatherArmorMeta) Helmet.getItemMeta();
		LeatherArmorMeta ches = (LeatherArmorMeta) Chest.getItemMeta();
		LeatherArmorMeta leg = (LeatherArmorMeta) leggings.getItemMeta();
		LeatherArmorMeta boot = (LeatherArmorMeta) boots.getItemMeta();

		hel.setDisplayName("" + ChatColor.WHITE + "IceArmor");
		ches.setDisplayName("" + ChatColor.WHITE + "IceArmor");
		leg.setDisplayName("" + ChatColor.WHITE + "IceArmor");
		boot.setDisplayName("" + ChatColor.WHITE + "IceArmor");
		hel.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true);
		boot.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true);
		ches.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true);
		leg.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true);
		hel.setColor(Color.WHITE);
		ches.setColor(Color.WHITE);
		leg.setColor(Color.WHITE);
		boot.setColor(Color.WHITE);

		Helmet.setItemMeta(hel);
		Chest.setItemMeta(ches);
		leggings.setItemMeta(leg);
		boots.setItemMeta(boot);

		if (pl.getItemInHand().equals(IceShield())) {

			final ArrayList<ItemStack> Item = new ArrayList<>();
			Item.add(pl.getInventory().getHelmet());
			Item.add(pl.getInventory().getChestplate());
			Item.add(pl.getInventory().getLeggings());
			Item.add(pl.getInventory().getBoots());

			pl.getInventory().setHelmet(new ItemStack(Helmet));
			pl.getInventory().setChestplate(new ItemStack(Chest));
			pl.getInventory().setLeggings(new ItemStack(leggings));
			pl.getInventory().setBoots(new ItemStack(boots));
			pl.getInventory().remove(IceShield());
			pl.getLocation().getWorld().playSound(pl.getLocation(), Sound.ITEM_ARMOR_EQUIP_DIAMOND, 1f, 1f);
			pl.getLocation().getWorld().spawnParticle(Particle.CLOUD, pl.getLocation(), 20, 0.5, 1, 0.5, 0.01);
			new BukkitRunnable() {

				@Override
				public void run() {
					pl.removePotionEffect(PotionEffectType.ABSORPTION);

					pl.getInventory().setHelmet(new ItemStack(Material.AIR));
					pl.getInventory().setChestplate(new ItemStack(Material.AIR));
					pl.getInventory().setLeggings(new ItemStack(Material.AIR));
					pl.getInventory().setBoots(new ItemStack(Material.AIR));

					// ---- Vai na gambi msm
					pl.getInventory().setHelmet(Item.get(0));
					pl.getInventory().setChestplate(Item.get(1));
					pl.getInventory().setLeggings(Item.get(2));
					pl.getInventory().setBoots(Item.get(3));

					pl.getLocation().getWorld().spawnParticle(Particle.SNOWBALL, pl.getLocation().add(0, 1, 0), 30, 0.5,
							1, 0.5, 0.01);
					pl.getLocation().getWorld().playSound(pl.getLocation(), Sound.BLOCK_ANVIL_PLACE, 1f, 1f);
					pl.getInventory().addItem(IceShield());
					Item.clear();
				}
			}.runTaskLater(plugin, 7 * 20);

		}
		} else {
			Eventos.lowMana(0.35F, pl, IceShield());
		}
	}


	public static  void HitSnow(ProjectileHitEvent e) {
		Projectile pj = e.getEntity();

		if (e.getEntity().getCustomName() == "Snow") {
			Player pl = (Player) e.getEntity().getShooter();
			
			
			pl.getWorld().spawnParticle(Particle.CLOUD, pj.getLocation().getX(), pj.getLocation().getY(),
					pj.getLocation().getZ(), 1, 0.4, 0, 0.4, 0.01);

			for (Entity ent : pj.getNearbyEntities(1, 1, 1)) {
				if (ent instanceof LivingEntity && !(Eventos.checkteam(pl).contains(ent.getName()))) {
					((LivingEntity) ent).damage(4, pl);
					pl.getWorld().spawnParticle(Particle.REDSTONE, pj.getLocation().getX(), pj.getLocation().getY(),
							pj.getLocation().getZ(), 5, 0.4, 0, 0.4, 0);
				}
			}

		}

	}


	public  static void NoKnockBack(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			Player pl = (Player) e.getEntity();
			if (Eventos.ClasseIB.contains(e.getEntity().getUniqueId())) {
				pl.setVelocity(pl.getLocation().getDirection());
			}
		}
	}

}
