package me.alpha.spell;

import me.alpha.spell.QuiverThief.quiverthief;
import me.alpha.spell.wither.*;
import net.md_5.bungee.api.ChatColor;
import me.alpha.spell.ChickenMaster.*;
import me.alpha.spell.IceBeast.icebeast;
import me.alpha.spell.burninglover.burninglover;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

public class Eventos implements Listener {

	public static main plugin = main.getPlugin(main.class);

	@EventHandler
	public void DropEvent(PlayerDropItemEvent e) {

		if (onClass(e.getPlayer()) && e.getPlayer().getGameMode() != GameMode.CREATIVE) {
			e.setCancelled(true);
		} else {
			return;
		}
	}

	@EventHandler
	public void Pickitem(PlayerPickupItemEvent e) {
		Player pl = e.getPlayer();

		if (onClass(pl) && pl.getGameMode() != GameMode.CREATIVE) {
			e.setCancelled(true);
		} 
	}

	@EventHandler
	public void SignInt(SignChangeEvent e) {
		if (e.getLine(0).equalsIgnoreCase("[Diamond]")) {
			e.setLine(0, "�b[Diamond]");
		}
		if (e.getLine(0).equalsIgnoreCase("[Gold]")) {
			e.setLine(0, "�6[Gold]");
		}
		if (e.getLine(0).equalsIgnoreCase("[Exit]")) {
			e.setLine(0, "�4[Exit]");
		}

		if (e.getLine(0).equalsIgnoreCase("[Wither]")) {
			e.setLine(0, "�5[Wither Mage]");
		}

		if (e.getLine(0).equalsIgnoreCase("[Chicken]")) {
			e.setLine(0, "�f[Chicken Master]");
		}
		if (e.getLine(0).equalsIgnoreCase("[Ice]")) {
			e.setLine(0, "�b[IceBeast]");
		}
		if (e.getLine(0).equalsIgnoreCase("[leave]")) {
			e.setLine(0, "�4[Leave class]");
		}
		if (e.getLine(0).equalsIgnoreCase("[lover]")) {
			e.setLine(0, "�d[BurningLover]");
		}
		if (e.getLine(0).equalsIgnoreCase("[Quiver]")) {
			e.setLine(0, "�8[Quiver Thief]");
		}
		if (e.getLine(0).equalsIgnoreCase("[Op]")) {
			e.setLine(0, "�f[Op]");
		}

	}

	public static ArrayList<Object> TeamD = new ArrayList<Object>();
	public static ArrayList<Object> TeamO = new ArrayList<Object>();

	public static ScoreboardManager manager = Bukkit.getScoreboardManager();
	public static Scoreboard score1 = manager.getNewScoreboard();
	public static Team teamDi = score1.registerNewTeam("�bDiamond");
	public static Team teamOu = score1.registerNewTeam("�6Ouro");
	public static Objective objective = score1.registerNewObjective("team", "dummy");

	@SuppressWarnings("deprecation")
	@EventHandler
	public void jointeam(PlayerInteractEvent e) {
		Player pl = e.getPlayer();

		if (!(e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_BLOCK))
			return;

		if (e.getClickedBlock().getState() instanceof Sign) {
			Sign s = (Sign) e.getClickedBlock().getState();

			if (s.getLine(0).equalsIgnoreCase("�b[Diamond]")) {

				if (TeamO.contains(e.getPlayer().getName())) {
					TeamO.remove(e.getPlayer().getName());
					teamOu.removePlayer(e.getPlayer());
				}

				TeamD.add(e.getPlayer().getName());
				teamDi.addPlayer(e.getPlayer());
				teamDi.setPrefix("�b");
				teamDi.setDisplayName("�bHonor");
				teamDi.setCanSeeFriendlyInvisibles(true);
				teamDi.setAllowFriendlyFire(false);
				e.getPlayer().sendMessage("" + ChatColor.AQUA + "" + ChatColor.BOLD + "You Join in Diamond ");

			} else if (s.getLine(0).equalsIgnoreCase("�6[Gold]")) {
				if (TeamD.contains(e.getPlayer().getName())) {
					TeamD.remove(e.getPlayer().getName());
					teamDi.removePlayer(e.getPlayer());
				}
				TeamO.add(e.getPlayer().getName());
				teamOu.addPlayer(e.getPlayer());

				teamOu.setPrefix("�6");
				teamOu.setDisplayName("�6Justice");
				teamOu.setCanSeeFriendlyInvisibles(true);
				teamOu.setAllowFriendlyFire(false);

				e.getPlayer().sendMessage("" + ChatColor.GOLD + "" + ChatColor.BOLD + "You Join in Gold");

			} else if (s.getLine(0).equalsIgnoreCase("�4[Exit]")) {
				teamDi.removePlayer(e.getPlayer());
				teamOu.removePlayer(e.getPlayer());
				TeamD.remove(e.getPlayer().getName());
				TeamO.remove(e.getPlayer().getName());
				Op.remove(pl.getUniqueId());
				e.getPlayer().sendMessage("" + ChatColor.RED + "" + ChatColor.BOLD + "You leave all team");
			} else if (s.getLine(0).equalsIgnoreCase("�5[Wither Mage]")) {
				SairDaClasse(e.getPlayer());
				Eventos.EscolherClasse(pl, Eventos.ClasseWM);
				PegarKit(e.getPlayer(), ClasseWM);
				e.getPlayer().sendMessage("" + ChatColor.GREEN + "" + ChatColor.BOLD + "You choose Wither Mage");

			} else if (s.getLine(0).equalsIgnoreCase("�f[Chicken Master]")) {
				SairDaClasse(e.getPlayer());
				Eventos.EscolherClasse(pl, Eventos.ClasseCM);
				PegarKit(e.getPlayer(), ClasseCM);
				e.getPlayer().sendMessage("" + ChatColor.GREEN + "" + ChatColor.BOLD + "You choose Chicken Master");
			} else if (s.getLine(0).equalsIgnoreCase("�b[IceBeast]")) {
				SairDaClasse(e.getPlayer());
				Eventos.EscolherClasse(pl, Eventos.ClasseIB);
				PegarKit(e.getPlayer(), ClasseIB);
				e.getPlayer().sendMessage("" + ChatColor.GREEN + "" + ChatColor.BOLD + "You choose IceBeast");
			} else if (s.getLine(0).equalsIgnoreCase("�4[Leave Class]")) {
				SairDaClasse(e.getPlayer());
				e.getPlayer().sendMessage("" + ChatColor.GREEN + "" + ChatColor.BOLD + "You leave your current class ");
			}else if (s.getLine(0).equalsIgnoreCase("�d[BurningLover]")){
				SairDaClasse(e.getPlayer());
				Eventos.EscolherClasse(pl, Eventos.ClasseBL);
				PegarKit(e.getPlayer(), ClasseBL);
				e.getPlayer().sendMessage("" + ChatColor.GREEN + "" + ChatColor.BOLD + "You choose BurningLover");
				
			}else if (s.getLine(0).equalsIgnoreCase("�8[Quiver Thief]")){
				SairDaClasse(e.getPlayer());
				Eventos.EscolherClasse(pl, Eventos.ClasseQT);
				PegarKit(e.getPlayer(), ClasseQT);
				e.getPlayer().sendMessage("" + ChatColor.GREEN + "" + ChatColor.BOLD + "You choose Quiver Thief");

			}
			else if (s.getLine(0).equalsIgnoreCase("�f[Op]")){
				Op.add(pl.getUniqueId());
				e.getPlayer().sendMessage("" + ChatColor.GREEN + "" + ChatColor.BOLD + "Enable No Cust Mana");
			}
		}

		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName("�fTeams");
		Score score = objective.getScore(e.getPlayer());
		score.setScore(42);
		e.getPlayer().setScoreboard(Eventos.score1);

	}

	public static ArrayList<Object> checkteam(Player pl) {

		if (TeamD.contains(pl.getName())) {
			return TeamD;
		} else {
			return TeamO;
		}

	}

	public static int noTeam(Player pl, Player ent) {

		if (TeamD.contains(pl.getName()) && TeamD.contains(ent.getName())) {
			return 1;
		} else if (TeamD.contains(pl.getName()) && TeamO.contains(ent.getName())) {
			return 2;
		} else if (!TeamD.contains(pl.getName()) && !TeamO.contains(ent.getName())) {
			return 3;
		} else if (TeamD.contains(pl.getName()) && !TeamO.contains(ent.getName())
				|| !TeamD.contains(pl.getName()) && TeamO.contains(ent.getName())) {
			return 4;
		}
		return 0;

	}

	// Classes Dispon?veis
	public static ArrayList<Object> ClasseWM = new ArrayList<>();
	public static ArrayList<Object> ClasseCM = new ArrayList<>();
	public static ArrayList<Object> ClasseIB = new ArrayList<>();
	public static ArrayList<Object> ClasseBL = new ArrayList<>();
	public static ArrayList<Object> ClasseQT = new ArrayList<>();


	// m?todo para Limpar todos os dados da classe.
	public static void clearClasses() {
		ClasseWM.clear();
		ClasseCM.clear();
		ClasseIB.clear();
		ClasseBL.clear();
		ClasseQT.clear();
	}

	// Verificar se o Player Esta em uma Classe.
	public static boolean onClass(Player pl) {

		if (ClasseWM.contains(pl.getUniqueId())) {
			return true;

		} else if (ClasseCM.contains(pl.getUniqueId())) {

			return true;

		} else if (ClasseIB.contains(pl.getUniqueId())) {
			return true;
		}else if (ClasseBL.contains(pl.getUniqueId())){
			return true;
		}else if (ClasseQT.contains(pl.getUniqueId())){
			return true;
		}
		return false;
	}

	@EventHandler
	private void CheckClass(PlayerMoveEvent e) {
		if (onClass(e.getPlayer()) == false) {
			for (PotionEffect pt : e.getPlayer().getActivePotionEffects()) {
				e.getPlayer().removePotionEffect(pt.getType());
			}
		}

	}

	// Adicionar um Jogador a uma classe
	public static void EscolherClasse(Player player, ArrayList<Object> classe) {
		classe.add(player.getUniqueId());
	}

	// remover um Jogador de uma classe
	public static void SairDaClasse(Player player) {
		if (ClasseWM.contains(player.getUniqueId())) {
			ClasseWM.remove(player.getUniqueId());
		}
		if (ClasseCM.contains(player.getUniqueId())) {
			ClasseCM.remove(player.getUniqueId());
		}
		if (ClasseIB.contains(player.getUniqueId())) {
			ClasseIB.remove(player.getUniqueId());

		}
		if (ClasseBL.contains(player.getUniqueId())) {
			ClasseBL.remove(player.getUniqueId());

		}
		if (ClasseQT.contains(player.getUniqueId())) {
			ClasseQT.remove(player.getUniqueId());

		}

		for (PotionEffect potion : player.getActivePotionEffects()) {
			player.removePotionEffect(potion.getType());

		}
		player.setMaxHealth(20);
		player.setHealth(player.getMaxHealth());
		player.getInventory().clear();
	}

	// Buscar o kit referente da classe
	public static void PegarKit(final Player pl, final ArrayList<Object> classe) {

		new BukkitRunnable() {

			@Override
			public void run() {
				pl.getInventory().removeItem(InventoryEvent.lobby());

				if (classe == ClasseWM) {
					pl.getInventory().addItem(WitherMage.Varinha());
					pl.getInventory().addItem(WitherMage.DefWither());
					pl.getInventory().addItem(WitherMage.EyesInvi());
				}

				if (classe == ClasseCM) {
					pl.getInventory().addItem(ChickenMaster.VarinhaOsso());
					pl.getInventory().addItem(ChickenMaster.heal());
					pl.getInventory().addItem(ChickenMaster.Fuga());
				}

				if (classe == ClasseIB) {
					pl.getInventory().addItem(icebeast.varinha());
					pl.getInventory().addItem(icebeast.IceField());
					pl.getInventory().addItem(icebeast.IceShield());
					pl.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE, 4), true);
					pl.setHealth(40);
				}
				if (classe == ClasseBL) {
					pl.getInventory().addItem(burninglover.staff());
					pl.getInventory().addItem(burninglover.Explosion());
					pl.getInventory().addItem(burninglover.DashFire());
				}
				if (classe == ClasseQT) {
					pl.getInventory().addItem(quiverthief.Staff());
					pl.getInventory().addItem(quiverthief.DashAtacck());
					pl.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1), true);
					pl.setMaxHealth(10);
				}
			}
		}.runTaskLater(plugin, 5);

	}
	static ArrayList<Object> Op = new ArrayList<>();
	// Descontar a mana para castar magias.
	public static void Mana(float mana, Player pl) {

		if(!Op.contains(pl.getUniqueId()))
		pl.setExp((pl.getExp() - mana));

	}

	@SuppressWarnings("deprecation")
	public static void lowMana(float mana, Player pl, ItemStack stack) {
		if (pl.getItemInHand().equals(stack) && pl.getExp() < mana) {
			pl.getInventory().removeItem(stack);
			pl.sendMessage("" + ChatColor.RED + "" + ChatColor.BOLD + "Does not have Enough Mana");
			pl.getInventory().addItem(stack);
		}
	}

}
