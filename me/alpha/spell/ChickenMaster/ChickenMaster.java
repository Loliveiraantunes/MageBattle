package me.alpha.spell.ChickenMaster;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import me.alpha.spell.Eventos;
import me.alpha.spell.FireWorkEvents;
import me.alpha.spell.main;

public class ChickenMaster implements Listener {

	public static main plugin = main.getPlugin(main.class);

	public static ItemStack VarinhaOsso() {
		ItemStack Staff = new ItemStack(Material.BONE);
		ItemMeta meta = Staff.getItemMeta();
		meta.setDisplayName("" + ChatColor.WHITE + "" + ChatColor.BOLD + "Bone Staff");
		meta.addEnchant(Enchantment.DAMAGE_ALL, 3, true);
		meta.addEnchant(Enchantment.KNOCKBACK, 3, true);
		Staff.setItemMeta(meta);
		return Staff;
	}

	public static ItemStack heal() {
		ItemStack HealS = new ItemStack(Material.EMERALD);
		ItemMeta HealM = HealS.getItemMeta();
		HealM.setDisplayName("" + ChatColor.GREEN + "" + ChatColor.BOLD + "Heal");
		HealM.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 2, true);
		HealS.setItemMeta(HealM);
		return HealS;
	}

	public static ItemStack Fuga() {
		ItemStack HealS = new ItemStack(Material.FEATHER);
		ItemMeta HealM = HealS.getItemMeta();
		HealM.setDisplayName("" + ChatColor.WHITE + "" + ChatColor.BOLD + "Scape");
		HealM.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 2, true);
		HealS.setItemMeta(HealM);
		return HealS;
	}

	public static ItemStack Pena() {
		ItemStack PenaS = new ItemStack(Material.FEATHER);
		ItemMeta PenaSM = PenaS.getItemMeta();
		PenaS.setItemMeta(PenaSM);
		return PenaS;
	}


    @SuppressWarnings("deprecation")
	public static void SecondSkill(final Player pl, Action action){
		if (pl.getItemInHand().equals(heal()) && pl.getExp() >= 0.25 && !(action == Action.LEFT_CLICK_AIR)
				&& !(action == Action.LEFT_CLICK_BLOCK)) {
			Eventos.Mana(0.25F, pl);
			final Location loc = new Location(pl.getWorld(), pl.getLocation().getX(), pl.getLocation().getY(),
					pl.getLocation().getZ());

			new BukkitRunnable() {
				int i = 0;

				@Override
				public void run() {
					pl.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc.getX(), loc.getY(), loc.getZ(), 100, 2,
							0.1, 2, 0.01);

					for (Player ent : Bukkit.getOnlinePlayers()) {

						if (ent.getLocation().distance(loc) <= 3 && ent.getHealth() < ent.getMaxHealth()) {

							switch (Eventos.noTeam(pl, ent)) {
								case 1:
									heal(pl, ent);
									break;

								case 3:
									heal(pl, ent);
									break;
								case 4:
									heal(pl, ent);
									break;

								default:
									break;
							}

						}

						pl.playSound(loc, Sound.ENTITY_CHICKEN_STEP, 1f, 1f);
					}

					if (i < 20) {
						i++;
					} else {
						this.cancel();
					}
				}
			}.runTaskTimer(plugin, 0, 10);

		} else {
			Eventos.lowMana(0.25F, pl, heal());
		}
	}



	@SuppressWarnings("deprecation")
	public  static void PrimaryAttack(Player pl, Action action){
		if (pl.getItemInHand().equals(VarinhaOsso()) && pl.getExp() >= 0.15 && !(action == Action.LEFT_CLICK_AIR)
				&& !(action == Action.LEFT_CLICK_BLOCK)) {
			Eventos.Mana(0.15F, pl);
			Egg arrow = pl.launchProjectile(Egg.class);
			arrow.setVelocity(pl.getEyeLocation().getDirection().multiply(2));
			arrow.setCustomName("Sumir");
			pl.playSound(pl.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1f, 1f);
		} else {
			Eventos.lowMana(0.15F, pl, VarinhaOsso());
		}
	}


	public static  void Third(){

	}


    @SuppressWarnings("deprecation")
	public static void Fuga(final Player pl,Action action){
        if ((pl.getItemInHand().equals(Fuga())) && (pl.getExp() >= 0.35D) && (action != Action.LEFT_CLICK_AIR) &&
                (action != Action.LEFT_CLICK_BLOCK))
        {
            Eventos.Mana(0.35F, pl);
            pl.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 3), true);
            pl.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 80, 1), true);
            for (int i = 0; i < 20; i++)
            {
                Random rand = new Random();
                pl.getLocation().getWorld().dropItem(pl
                        .getLocation().add(new Vector(rand.nextInt(2), rand.nextInt(3), rand.nextInt(2))), Pena());
            }
            pl.getWorld().playSound(pl.getLocation(), Sound.ENTITY_CHICKEN_DEATH, 1.0F, 1.0F);
            pl.getLocation().getWorld().spawnParticle(Particle.CLOUD, pl.getLocation(), 3, 0.5D, 1.0D, 0.5D, 0.01D);
            pl.getInventory().remove(Fuga());
            pl.getWorld().playSound(pl.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 0.5F, 0.5F);

            int tempo = 4;
            new BukkitRunnable()
            {
                int Do = 0;
                Random rand = new Random();

                public void run()
                {
                    pl.getLocation().getWorld().spawnParticle(Particle.CLOUD, pl.getLocation(), 1, 0.0D, 0.0D, 0.0D, 0.01D);
                    pl.getLocation().getWorld().dropItem(pl
                                    .getLocation().add(new Vector(this.rand.nextInt(2), this.rand.nextInt(3), this.rand.nextInt(2))),
                            ChickenMaster.Pena());
                    this.Do += 1;
                    if (this.Do > 40) {
                        cancel();
                    }
                }
            }

                    .runTaskTimer(plugin, 0L, 2L);

            new BukkitRunnable()
            {
                public void run()
                {
                    pl.removePotionEffect(PotionEffectType.SPEED);
                    pl.getInventory().addItem(new ItemStack[] { ChickenMaster.Fuga() });
                    pl.getWorld().playSound(pl.getLocation(), Sound.BLOCK_PORTAL_TRAVEL, 0.5F, 0.5F);
                }
            }

                    .runTaskLater(plugin, 80L);
        }
        else
        {
            Eventos.lowMana(0.35F, pl, Fuga());
        }
    }




	public static void heal(Player pl, Player ent) {
		float heal = 1F;

		if (Eventos.ClasseIB.contains(ent.getUniqueId()) && ent.getHealth() < ent.getMaxHealth()) {
			heal = 1F;
		}
		if (ent.getHealth() < ent.getMaxHealth()) {

			if (ent.getHealth() + heal > ent.getMaxHealth()) {
				heal = 0.05F;
			}
			ent.setHealth(ent.getHealth() + heal);

			pl.getWorld().spawnParticle(Particle.HEART, ent.getLocation().getX(), ent.getLocation().getY(),
					ent.getLocation().getZ(), 10, 0.5, 1, 0.5, 0.01);

			pl.playSound(ent.getLocation(), Sound.ENTITY_CHICKEN_HURT, 1f, 1f);
		}

	}





	public static void EggDamage(ProjectileHitEvent e) {
		Entity pl = (Entity) e.getEntity().getShooter();
		Projectile pj = e.getEntity();
		if (pj.getType() == EntityType.EGG && pj.getCustomName() == "Sumir") {
			if (pl instanceof LivingEntity) {
				pj.getLocation().getWorld().spawnParticle(Particle.CLOUD, pj.getLocation().getX(),
						pj.getLocation().getY(), pj.getLocation().getZ(), 5, 0.3, 1, 0.3, 0.01);
				FireWorkEvents.FwHitEgg(pj.getLocation());
				Random rand = new Random();

				for (int i = 0; i < 20; i++) {
					pj.getLocation().getWorld().dropItem(
							pj.getLocation().add(new Vector(rand.nextInt(2), rand.nextInt(3), rand.nextInt(2))),
							Pena());
					pj.getLocation().getWorld().playSound(pj.getLocation(), Sound.ENTITY_PLAYER_SWIM, 0.1F, 0.1f);

					for (Entity ent : pj.getNearbyEntities(2, 1.5, 2)) {

						if (ent instanceof LivingEntity && pl instanceof Player) {
							if (!(Eventos.checkteam((Player) pl).contains(ent.getName()))) {

								((LivingEntity) ent).damage(6, pl);

							}

						}
					}

				}
			}
		}

	}


	public  static void ItemDrop(final ItemSpawnEvent e) {
		if (e.getEntity().getItemStack().isSimilar(Pena())) {

			new BukkitRunnable() {

				@Override
				public void run() {
					e.getEntity().remove();
				}
			}.runTaskLater(plugin, 25);
		}

	}


	public static void Nostack(ItemMergeEvent e) {
		if (e.getEntity().getItemStack().equals(Pena())) {
			e.setCancelled(true);
		}
	}


	public static void DropItem(PlayerPickupItemEvent e) {
		if (e.getItem().getItemStack().equals(Pena())) {
			e.setCancelled(true);
			e.getItem().remove();
		}

	}

}
