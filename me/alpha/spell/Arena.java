
package me.alpha.spell;

import org.bukkit.event.Listener;

import net.md_5.bungee.api.ChatColor;

public class Arena implements Listener {

	public static void CreateArena(String arena) {

		Config.config.set("mapa." + arena + ".spawn", 0);
		Config.saveYamls();

	}

	public static void DeletArena(String arena) {

		Config.config.set("mapa." + arena, null);
		Config.config.set("mapa." + arena + ".name", null);
		Config.config.set("mapa." + arena + ".spawn", null);
		Config.config.set("mapa." + arena + ".x", null);
		Config.config.set("mapa." + arena + ".y", null);
		Config.config.set("mapa." + arena + ".z", null);
		Config.saveYamls();

	}

	public static void addLoc(String arena) {
		/*
		 * double x = loc.getX(); double y = loc.getY(); double z = loc.getZ() ;
		 */

		int id = (int) Config.config.get("mapa." + arena + ".spawn") + 1;
		Config.config.set("mapa." + arena + ".spawn", id);
		Config.config.set("mapa." + arena + "." + id + ".x", 0);
		Config.config.set("mapa." + arena + "." + id + ".y", 0);
		Config.config.set("mapa." + arena + "." + id + ".z", 0);
		Config.saveYamls();
	}

	public static void ListLoc(String arena) {

		int cont = Config.config.getConfigurationSection("mapa." + arena).getKeys(false).size();
		int i = 1;
		int c = 1;
		while (c <= cont) {

			if (Config.config.get("mapa." + arena + "." + i) != null) {
				main.getPlugin(main.class).getServer().getLogger().info("" + ChatColor.GREEN + "-------------");
				main.getPlugin(main.class).getServer().getLogger()
						.info("" + ChatColor.GOLD + arena + " '" + i + "' x: "
								+ Config.config.get("mapa." + arena + "." + i + ".x") + " y: "
								+ Config.config.get("mapa." + arena + "." + i + ".y") + " z: "
								+ Config.config.get("mapa." + arena + "." + i + ".z"));
				main.getPlugin(main.class).getServer().getLogger().info("" + ChatColor.GREEN + "-------------");
				c++;
			}
			i++;
			if (c >= cont) {
				break;
			}

		}

	}

	public static void removeLoc(String arena) {
		int id = (int) Config.config.get("mapa." + arena + ".spawn") + 1;
		Config.config.set("mapa." + arena + ".spawn", id);
		Config.config.set("mapa." + arena + "." + id + ".x", 0);
		Config.config.set("mapa." + arena + "." + id + ".y", 0);
		Config.config.set("mapa." + arena + "." + id + ".z", 0);
		Config.saveYamls();
	}

	// Tenho que criar removeLoc por verificação por id com listagem dos ids....
	// Teleportar pra loc da Arena para ter certeza do lugar.

	public static void getLoc() {

		String arena = "Arena3";

		int id = (int) Config.config.get("mapa." + arena + ".spawn");
		double x = (double) Config.config.get("mapa." + arena + ".x");
		double y = (double) Config.config.get("mapa." + arena + ".y");
		double z = (double) Config.config.get("mapa." + arena + ".z");
		main.getPlugin(main.class).getServer().getLogger()
				.info("\nLoc: id: " + id + "  x:" + x + " y: " + y + " z: " + z);

	}

	public static void RemoveLoc() {

		String arena = "Arena1";
		Config.config.set("mapa." + arena, null);
		Config.config.set("mapa." + arena + ".spawn", null);
		Config.config.set("mapa." + arena + ".x", null);
		Config.config.set("mapa." + arena + ".y", null);
		Config.config.set("mapa." + arena + ".z", null);
		Config.saveYamls();

	}

}
