package me.alpha.spell;

import me.alpha.spell.ChickenMaster.ChickenMaster;
import me.alpha.spell.IceBeast.icebeast;
import me.alpha.spell.QuiverThief.quiverthief;
import me.alpha.spell.burninglover.burninglover;
import me.alpha.spell.wither.WitherMage;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class ListenersEvent implements Listener {

    private main plugin = main.getPlugin(main.class);


    @EventHandler
    private void DoEventsInteract(PlayerInteractEvent e){
        Player pl = e.getPlayer();
        Action action = e.getAction();
        //Burning Lover
        burninglover.PrimaryAtk(pl,action);
        burninglover.DashFire(pl,action,plugin);
        burninglover.SetDef(pl,action);

        //WitherMage
        WitherMage.PrimaryAttack(pl,action);
        WitherMage.SetInv(pl,action,plugin);
        WitherMage.SetDef(pl,action,plugin);
        // ChickenMaster
        ChickenMaster.PrimaryAttack(pl,action);
        ChickenMaster.SecondSkill(pl,action);
        ChickenMaster.Fuga(pl,action);
        //IceBeast

        icebeast.PrimaryAttack(pl,plugin,action);
        icebeast.SecondAttack(pl,plugin,action);
        icebeast.Setdef(pl,plugin,action);

        //QuiverThief
        quiverthief.Primaryattack(pl,action);
        quiverthief.DashAtack(pl,action,plugin);
    }

    @EventHandler
    private void DoLauchProjectile(ProjectileLaunchEvent e){
        Projectile pj = e.getEntity();
        if(pj.getShooter() instanceof  Player){
        Player pl = (Player) pj.getShooter();

        burninglover.ProjectFlying(pj,plugin);
        WitherMage.ProjetilVoando(pj,pl,plugin);
        quiverthief.ProjectileFlying(pj,plugin);
        }
    }


    @EventHandler
    private void DoHitEvent(ProjectileHitEvent e){
        burninglover.ProjectileHit(e,plugin);
        ChickenMaster.EggDamage(e);
        icebeast.HitSnow(e);
        WitherMage.projectHit(e,plugin);
        quiverthief.HitEvent(e);
    }


    @EventHandler
    private  void Doitemdrop(ItemSpawnEvent e){

        burninglover.ItemDrop(e,plugin);
        ChickenMaster.ItemDrop(e);

    }


    @EventHandler
    private  void DoDamageEntitybyEntity(EntityDamageByEntityEvent e){

    WitherMage.DanoComVarinha(e);
    quiverthief.EntityDamage(e);
    }


    @EventHandler
    private void DoGetdamage(EntityDamageEvent e){

        icebeast.NoKnockBack(e);

        quiverthief.damageEnt(e)
        ;

    }


    @EventHandler
    private  void DoNotMergeItem(ItemMergeEvent e){
        burninglover.Nostack(e);
        ChickenMaster.Nostack(e);

    }


    @EventHandler
    private  void DoNoPickItem(PlayerPickupItemEvent e){
        burninglover.NoPick(e);
        ChickenMaster.DropItem(e);
    }


    @EventHandler
    public void NoSpawn(CreatureSpawnEvent e) {

        if (e.getSpawnReason() == CreatureSpawnEvent.SpawnReason.EGG) {
            e.setCancelled(true);
        }
    }


}
