package me.alpha.spell;

import java.util.ArrayList;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import me.alpha.spell.ChickenMaster.ChickenMaster;
import me.alpha.spell.IceBeast.icebeast;
import me.alpha.spell.burninglover.burninglover;
import me.alpha.spell.wither.WitherMage;
import net.md_5.bungee.api.ChatColor;

public class util implements Listener {

	private static main plugin = main.getPlugin(main.class);




	@EventHandler
	public void LoadCooldown(PlayerMoveEvent e) {

		Player pl = e.getPlayer();
		pl.setFoodLevel(20);

		if (pl.getLocation().getBlockY() < 1 && pl.getGameMode() != GameMode.CREATIVE) {
			pl.damage(40);

			plugin.getServer().broadcastMessage(""+ org.bukkit.ChatColor.GREEN+" "+pl.getName()+ org.bukkit.ChatColor.RED+" was killed by "+ org.bukkit.ChatColor.GREEN+"Void");
			pl.playSound(pl.getLocation(), Sound.BLOCK_ANVIL_DESTROY, 2F, 2f);
		}

		if (!main.PlayerList.contains(pl.getUniqueId())) {
			main.PlayerList.add(pl.getUniqueId());
			cooldown.cooldownXp(pl, plugin);

		}

	}


	@EventHandler
	public void ChangeStatsFly(PlayerToggleFlightEvent e) {
		Player pl = e.getPlayer();
		if (pl.getGameMode() == GameMode.CREATIVE )
			return;
		e.setCancelled(true);
		pl.setAllowFlight(false);
		pl.setFlying(false);
		if(!Eventos.ClasseIB.contains(pl.getUniqueId()))
		pl.setVelocity(pl.getLocation().getDirection().multiply(0.6).setY(0.8));
	}

	static ArrayList<Object> FallCt = new ArrayList<>();

	public void FallControl(final Player pl) {
		new BukkitRunnable() {
			@Override
			public void run() {
				pl.setAllowFlight(false);
				FallCt.remove(pl.getUniqueId());
			}
		}.runTaskLater(plugin, 2 * 20);
	}

	@EventHandler
	public void Nofalldamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			if (e.getCause() == DamageCause.FALL) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void DoubleJump(PlayerMoveEvent e) {
		Player pl = e.getPlayer();

		if (pl.getGameMode() != GameMode.CREATIVE && !(pl.isFlying())
				&& !(pl.getLocation().add(new Vector(0, -1, 0)).getBlock().getType() == Material.AIR)) {
			if (!Eventos.ClasseIB.contains(pl.getUniqueId())) {
				pl.setAllowFlight(true);
			}
		}

	}

	@EventHandler
	public void Nobreak(BlockBreakEvent e) {
		Player pl = e.getPlayer();
		if (!pl.getGameMode().equals(GameMode.CREATIVE)) {
			e.setCancelled(true);
		}

	}

	@EventHandler
	public void PutBlock(BlockPlaceEvent e) {
		Player pl = e.getPlayer();
		if (!pl.getGameMode().equals(GameMode.CREATIVE)) {
			e.setCancelled(true);
		}

	}

	@EventHandler
	public void MensageDeath(EntityDamageByEntityEvent e) {
		Entity pl = e.getEntity();
		if (pl instanceof Player) {
			Player player = (Player) pl;
			if (player.getHealth() <= e.getDamage())

				if (e.getCause() == DamageCause.ENTITY_ATTACK || e.getCause() == DamageCause.ENTITY_EXPLOSION) {
					plugin.getServer().broadcastMessage("" + ChatColor.GREEN + " » " + pl.getName() + ChatColor.RED
							+ " was killed by " + ChatColor.GREEN + e.getDamager().getName());
				} else if (e.getCause() == DamageCause.PROJECTILE) {
					Projectile projectile = (Projectile) e.getDamager();
					player = (Player) projectile.getShooter();
					plugin.getServer().broadcastMessage("" + ChatColor.GREEN + " » " + pl.getName() + ChatColor.RED
							+ " was killed by " + ChatColor.GREEN + player.getName());

				}

		}

	}

	@EventHandler(priority = EventPriority.HIGH)
	public void Nodie(EntityDamageEvent e) {


		


		final Entity pl = e.getEntity();
		if (pl instanceof Player) {
			
			if(e.getCause()==DamageCause.FALL){
				e.setCancelled(true);
				
			}
			
			
			if (((Damageable) pl).getHealth() <= e.getDamage()) {

				e.setCancelled(true);


				pl.teleport(pl.getWorld().getSpawnLocation());
				((Player) pl).setExp(0.5F);
				((Damageable) pl).setHealth(((Player) pl).getMaxHealth());



				((Player) pl).setFoodLevel(20);
				((Player) pl).setLevel(0);

				if (!Eventos.onClass((Player) pl)) {
					((Player) pl).getInventory().clear();
				}

				for (PotionEffect potion : ((Player) pl).getActivePotionEffects()) {

					((Player) pl).removePotionEffect(potion.getType());

					if (Eventos.ClasseIB.contains(pl.getUniqueId())) {
						((LivingEntity) pl).addPotionEffect(
								new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE, 4), true);
						((Damageable) pl).setHealth(((Player) pl).getMaxHealth());
					}

					if (Eventos.ClasseQT.contains(pl.getUniqueId())) {
						((LivingEntity) pl).addPotionEffect(
								new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2), true);
						((Damageable) pl).setHealth(((Player) pl).getMaxHealth());
					}

				}
				new BukkitRunnable(){
					@Override
					public void run(){

						pl.setFireTicks(0);}

				}.runTaskLater(plugin,1);


			}
		}
	}



	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.HIGH)
	public void debug(PlayerInteractEvent e) {
		final Player pl = e.getPlayer();
		if (pl.getGameMode() != GameMode.CREATIVE) {
			if (!(e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK)) {
				if(pl.getItemInHand().equals(ChickenMaster.VarinhaOsso()) || pl.getItemInHand().equals(icebeast.varinha())|| pl.getItemInHand().equals(WitherMage.Varinha())|| pl.getItemInHand().equals(burninglover.staff()) ){
					
				
				final ItemStack item = pl.getItemInHand();
				pl.getInventory().remove(item);
				new BukkitRunnable() {

					@Override
					public void run() {

						pl.getInventory().addItem(item);
					}
				}.runTaskLater(plugin, 1);
		   	}
		 }
		}
	}

}
